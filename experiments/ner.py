from experiments.word2vect import Word2Vect
from collections import namedtuple
import numpy as np
from core import birnnmodel as Model
import re

words = namedtuple('Words', 'word tag vector')

tags = {
    'O': 0,
    'B-person': 1,
    'I-person': 2,
    'B-geo-loc': 3,
    'I-geo-loc': 4,
    'B-company': 5,
    'I-company': 6
}
tags_notypes = {
    'O': 0,
    'B': 1,
    'I': 2
}

random_vector = dict()


# B -  198 # 139 # 198
# I -  205 # 417 # 205

class NER:
    def __init__(self, model=None):
        self.train_file = open('../data/source/train_types.txt', 'r')
        self.test_file = open('../data/source/test_types.txt', 'r')
        self.dev_file = open('../data/source/dev_types.txt', 'r')
        self.word2vect = Word2Vect(model=model)

    def get_data(self, file):
        vectors = []
        sentence = []
        dataX = []
        dataY = []
        x = []
        y = []
        for line in file:
            value = line.strip().split('\t')
            if line != '\n' and len(value) < 2:
                print(line, value)
            if len(value) == 2:
                word = value[0]
                if word in self.word2vect.model:
                    word_vector = self.word2vect.model[word]
                else:
                    word_vector = self.gen_vector(word)
                label = 0 if value[1] not in tags else tags[value[1]]
                x.append(word_vector)
                y.append(label)
                sentence.append(words(word=word, tag=label, vector=word_vector))
            if line == '\n':
                vectors.append(sentence)
                dataX.append(x)
                dataY.append(y)
                sentence = []
                x = []
                y = []
        print('done')
        dataX = list(filter(None, dataX))
        dataY = list(filter(None, dataY))
        return np.array(dataX), np.array(dataY)

    def gen_vector(self, word):
        if word not in random_vector:
            random_vector[word] = np.random.uniform(-1, 1, (300,))
        return random_vector[word]

    def test(self):
        vectors = []
        for line in self.ner:
            value = line.strip().split('\t')
            if len(value) == 2:

                if value[0] in self.word2vect.model:
                    vectors.append(words(word=value[0], tag=value[1], vector=self.word2vect.model[value[0]]))
        print('B-person: {}'.format(len(list(filter(lambda x: x[1].startswith('B-person'), vectors)))))
        print('B-geo-loc: {}'.format(len(list(filter(lambda x: x[1].startswith('B-geo-loc'), vectors)))))
        print('B-company: {}'.format(len(list(filter(lambda x: x[1].startswith('B-company'), vectors)))))
        print('B-other: {}'.format(len(list(filter(lambda x: x[1].startswith('B-other'), vectors)))))
        print('O: {}'.format(len(list(filter(lambda x: x[1].startswith('O'), vectors)))))

        print('I-person: {}'.format(len(list(filter(lambda x: x[1].startswith('I-person'), vectors)))))
        print('I-geo-loc: {}'.format(len(list(filter(lambda x: x[1].startswith('I-geo-loc'), vectors)))))
        print('I-company: {}'.format(len(list(filter(lambda x: x[1].startswith('I-company'), vectors)))))
        print('I-other: {}'.format(len(list(filter(lambda x: x[1].startswith('I-other'), vectors)))))

        print('done')
#
#
#ner_obj = NER(model='wordvect')
# trainX, trainY = ner_obj.get_data(ner_obj.train_file)
#testX, testY = ner_obj.get_data(ner_obj.test_file)
#devX, devY = ner_obj.get_data(ner_obj.dev_file)
#
# print('Train file length: {}'.format(trainX.size))
#print('Test file length: {}'.format(testX.size))
#print('Dev file length: {}'.format(devX.size))
