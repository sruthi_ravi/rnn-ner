import numpy as np
from experiments import helper
from sklearn.metrics import confusion_matrix
from sklearn.metrics import precision_score, recall_score


def predict(x_test, y_test):
    # read the parameters from the numpy file and do the forward propagation

    weights = {
        'u': np.load(helper.parameter_file['u']),
        'v': np.load(helper.parameter_file['v']),
        'w': np.load(helper.parameter_file['w']),
        'u_bi': np.load(helper.parameter_file['u_bi']),
        'w_bi': np.load(helper.parameter_file['w_bi']),
        'v_bi': np.load(helper.parameter_file['v_bi']),
        'fc_b': np.load(helper.parameter_file['fc_b']),
        'fc_w': np.load(helper.parameter_file['fc_w']),
        'fc_h': np.load(helper.parameter_file['fc_h']),
        'fc_hb': np.load(helper.parameter_file['fc_hb'])

    }
    rnn = helper.init_model(weights)
    accurate = 0
    t_pred = 0
    t_actu = 0
    inaccuracte = 0

    with open('../output/ner_output.txt', 'w') as fw:
        for i in range(len(x_test)):
            output = rnn.predict(x_test[i])
            p, r, correct, wrong = get_metrics(np.array(y_test[i]), output)
            t_pred += p
            t_actu += r
            accurate += correct
            inaccuracte +=wrong
        precision = accurate / t_pred
        recall = accurate / t_actu
        f1 = 2 * precision * recall / (precision + recall)
        fw.write(
            'F1 score : {}'.format(f1 * 100))
        fw.write(
            'Precision score : {}'.format(precision * 100))
        fw.write(
            'Recall score : {}'.format(recall * 100))
    print(
        'F1 score : {}'.format(f1 * 100))
    print(
        'Precision score : {}'.format(precision * 100))
    print(
        'Recall score : {}'.format(recall * 100))


def get_metrics(y_actu, y_pred):
    correct = 0
    wrong =0
    c_pred = np.count_nonzero(y_pred)
    c_actu = np.count_nonzero(y_actu)
    for x, y in np.nditer([y_actu, y_pred]):
        if x != 0 and y != 0 and x == y:
            correct += 1
        if x != 0  and y != 0 and x != y:
            wrong +=1
    return c_pred, c_actu, correct, wrong


def main():
    x_test = np.load(helper.data_path['x_test'])
    y_test = np.load(helper.data_path['y_test'])
    print('Testing....')
    predict(x_test, y_test)


if __name__ == '__main__':
    main()
