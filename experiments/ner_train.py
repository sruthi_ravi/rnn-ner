import numpy as np
from experiments import helper
import timeit

def main():
    rnn = helper.init_model()
    x_train = np.load(helper.data_path['x_train'])
    y_train = np.load(helper.data_path['y_train'])
    # # helper.one_training(x_train[0], rnn)
    rnn.train(x_train, y_train, nepoch=50, evaluate_loss_after=1)

    print('Saving model parameters....')

    np.save(helper.parameter_file['u'], rnn.U)
    np.save(helper.parameter_file['w'], rnn.W)
    np.save(helper.parameter_file['v'], rnn.V)
    np.save(helper.parameter_file['u_bi'], rnn.U_bi)
    np.save(helper.parameter_file['w_bi'], rnn.W_bi)
    np.save(helper.parameter_file['v_bi'], rnn.V_bi)
    np.save(helper.parameter_file['fc_w'], rnn.fc_layer.params['fc_test_w'])
    np.save(helper.parameter_file['fc_b'], rnn.fc_layer.params['fc_test_b'])
    np.save(helper.parameter_file['fc_h'], rnn.fc_layer.params['fc_test_h'])
    np.save(helper.parameter_file['fc_hb'], rnn.fc_layer.params['fc_test_hb'])

# if __name__ == '__main__':
#
#     main()

if __name__ == '__main__':
    exec_time = '{:.2f}s'.format(timeit.timeit("main()", setup="from ner_train import main", number=1))
    print(exec_time)
