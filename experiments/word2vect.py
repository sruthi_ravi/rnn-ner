from gensim.scripts.glove2word2vec import glove2word2vec
from gensim.models import KeyedVectors


class Word2Vect:
    # load the Stanford GloVe model
    def __init__(self, model=None):
        if model == 'glove':
            self.load_glove()
        if model == 'wordvect':
            self.load_wordvect()
        if model == 'twitter':
            self.load_twitter()

    def load_glove(self):
        glove_input_file = '/Users/sruthiravi/DR/dataset/glove/glove.6B.100d.txt'
        word2vec_output_file = '/Users/sruthiravi/DR/dataset/glove/glove.6B.100d.txt.word2vec'
        glove2word2vec(glove_input_file, word2vec_output_file)
        filename = '/Users/sruthiravi/DR/dataset/glove/glove.6B.100d.txt.word2vec'
        self.model = KeyedVectors.load_word2vec_format(filename, binary=False)

    def load_wordvect(self):
        filename = '/Users/sruthiravi/DR/dataset/GoogleNews-vectors-negative300.bin'
        self.model = KeyedVectors.load_word2vec_format(filename, binary=True)

    def load_twitter(self):
        glove_input_file = '/Users/sruthiravi/DR/dataset/glove-twitter/glove.twitter.27B.50d.txt'
        word2vec_output_file = '/Users/sruthiravi/DR/dataset/glove-twitter/glove.twitter.27B.50d.txt.word2vec'
        glove2word2vec(glove_input_file, word2vec_output_file)
        filename = '/Users/sruthiravi/DR/dataset/glove-twitter/glove.twitter.27B.50d.txt.word2vec'
        self.model = KeyedVectors.load_word2vec_format(filename, binary=False)

    def test(self):
        # calculate: (king - man) + woman = ?
        result = self.model.most_similar(positive=['woman', 'king'], negative=['man'], topn=1)
        print(self.model['sentence'])
        print(result)

# wordvect = Word2Vect()
# wordvect.test()
