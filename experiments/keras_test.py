# load json and create model
from keras.models import model_from_json
from experiments import helper
import numpy as np
from experiments.keras_train import load_data
from experiments import ner_test
x_train, y_train, x_test, y_test = load_data()
y_actu = np.load('../data/gen/testY_notypes.npy')

json_file = open('../parameters/keras_model_ner.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
loaded_model = model_from_json(loaded_model_json)
# load weights into new model
loaded_model.load_weights("../parameters/keras_model_ner.h5")
print("Loaded model from disk")

# evaluate loaded model on test data
loaded_model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
for layer in loaded_model.layers:
    config = layer.get_config()
    weights = layer.get_weights()
    print(config)
    print(len(weights))

accurate = 0
t_pred = 0
t_actu = 0
for i in range(len(x_test)):
    score = loaded_model.evaluate(np.array(x_test[i]).reshape(1, len(x_test[i]), 300),
                                  np.array(y_test[i]).reshape(1, len(y_test[i]), 3), verbose=0)
    ypredict = loaded_model.predict_classes(np.array(x_test[i]).reshape(1, len(x_test[i]), 300), verbose=0)

    p, r, correct = ner_test.get_metrics(np.array(y_actu[i]), ypredict.reshape(len(y_actu[i])))
    t_pred += p
    t_actu += r
    accurate += correct
precision = accurate / t_pred
recall = accurate / t_actu
f1 = 2 * precision * recall / (precision + recall)

print(
    'F1 score : {}'.format(f1 * 100))
print(
    'Precision score : {}'.format(precision * 100))
print(
    'Recall score : {}'.format(recall * 100))



print("%s: %.2f%%" % (loaded_model.metrics_names[1], score[1] * 100))
print(loaded_model.get_layer(name="bidirectional_1").input_shape)
print(loaded_model.get_layer(name="bidirectional_1").output_shape)
# print(loaded_model.get_layer(name="lstm_1").input_shape)
# print(loaded_model.get_layer(name="lstm_1").output_shape)
print(loaded_model.get_layer(name="time_distributed_1").input_shape)
print(loaded_model.get_layer(name="time_distributed_1").output_shape)
# print(loaded_model.get_layer(name="dense_1").input_shape)
# print(loaded_model.get_layer(name="dense_1").output_shape)
