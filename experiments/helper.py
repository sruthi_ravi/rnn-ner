# ner = open('../data/ner_tagged_2.txt', 'r')
# tag_dict = {}
# for line in ner:
#     value = line.strip().split('\t')
#     if len(value) == 2:
#         tag = value[1]
#         if tag not in tag_dict:
#             tag_dict[tag] = 0
#         tag_dict[tag] += 1
# print(tag_dict)


from core.bi_rnn_v2 import BiRNN as Model
from core.activation import Softmax
from core.activation import Sigmoid
from core.activation import SigmoidwithCrossEntropy
import numpy as np
from experiments.ner import NER

parameter_file = {
    'u': '../parameters/u.npy',
    'v': '../parameters/v.npy',
    'w': '../parameters/w.npy',
    'u_bi': '../parameters/u_bi.npy',
    'w_bi': '../parameters/w_bi.npy',
    'v_bi': '../parameters/v_bi.npy',
    'fc_w': '../parameters/fc_w.npy',
    'fc_b': '../parameters/fc_b.npy',
    'fc_h': '../parameters/fc_h_.npy',
    'fc_hb': '../parameters/fc_hb.npy',

}
data_path = {
    'x_train': '../data/gen/trainX_notypes.npy',
    'y_train': '../data/gen/trainY_notypes.npy',
    'x_test': '../data/gen/testX_notypes.npy',
    'y_test': '../data/gen/testY_notypes.npy',
    'x_dev': '../data/gen/devX_notypes.npy',
    'y_dev': '../data/gen/devY_notypes.npy'
}


def get_data():
    ner_obj = NER(model='wordvect')
    x_train, y_train = ner_obj.get_data(ner_obj.train_file)
    x_test, y_test = ner_obj.get_data(ner_obj.test_file)
    x_dev, y_dev = ner_obj.get_data(ner_obj.dev_file)
    print(x_train.size)
    print(y_train[-1])
    print(x_test.size)
    print(y_test[-1])
    print(x_dev.size)
    print(y_dev[-1])
    write_to_file(x_train, y_train, x_test, y_test, x_dev, y_dev)
    return x_train, y_train, x_test, y_test


def word_vector(z, size):
    binary = '{0:07b}'.format(z)
    return np.array([int(val) for val in binary])


def generator(z, size):
    return z


def write_output(file, data):
    with open(file, 'w') as fw:
        for val in data:
            fw.write(','.join(str(v) for v in val) + '\n')
    fw.close()


def write_to_file(x_train, y_train, x_test, y_test, x_dev, y_dev):
    np.save(data_path['x_train'], x_train)
    np.save(data_path['y_train'], y_train)
    np.save(data_path['x_test'], x_test)
    np.save(data_path['y_test'], y_test)
    np.save(data_path['x_dev'], x_dev)
    np.save(data_path['y_dev'], y_dev)
    # write_output(data_path['x_train'], x_train)
    # write_output(data_path['y_train'], y_train)
    # write_output(data_path['x_test'], x_test)
    # write_output(data_path['y_test'], y_test)


def init_model(weights={}):
    word_dim = 300
    hidden_dim = 200
    output_dim = 3
    return Model(word_dim, output_dim, Softmax, generator, hidden_dim=hidden_dim, bptt_truncate=0,
                 learning_rate=0.005, optimizer=None,
                 update_type='sequence', weights=weights)
#get_data()

def load_weight():
    path = '/Users/sruthiravi/DR/rnn-ner/parameters/fc_5_ss_h_5.npy'
    w = np.load(path)
    print('done')

#load_weight()