from random import random
import numpy as np
import timeit
from matplotlib import pyplot
from pandas import DataFrame
from keras.models import Sequential
from keras.layers import LSTM
from keras.layers import SimpleRNN
from keras.layers import Dense
from keras.layers import TimeDistributed
from keras.layers import Bidirectional
from keras.utils import to_categorical
from sklearn.preprocessing import LabelEncoder
from keras.preprocessing import sequence
from experiments import helper


def load_data():
    x_train = np.load('../data/gen/trainX_notypes.npy')
    y_train = np.load('../data/gen/trainY_keras.npy')
    x_test = np.load('../data/gen/testX_notypes.npy')
    y_test = np.load('../data/gen/testY_keras.npy')
    return x_train, y_train, x_test, y_test


def get_lstm_model(n_timesteps, backwards):
    model = Sequential()
    model.add(LSTM(20, input_shape=(n_timesteps, 1), return_sequences=True, go_backwards=backwards))
    model.add(TimeDistributed(Dense(1, activation='sigmoid')))
    model.compile(loss='binary_crossentropy', optimizer='adam')
    return model


def get_bi_lstm_model(n_timesteps, mode):
    model = Sequential()
    model.add(Bidirectional(SimpleRNN(20, return_sequences=True), input_shape=(None, 300), merge_mode=mode))
    model.add(TimeDistributed(Dense(3, activation='softmax')))
    model.add((Dense(3, activation='softmax')))
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    # print(model.get_layer(name="bidirectional_1").input_shape)
    # print(model.get_layer(name="bidirectional_1").output_shape)
    # print(model.get_layer(name="time_distributed_1").input_shape)
    # print(model.get_layer(name="time_distributed_1").output_shape)
    # print(model.get_layer(name="dense_2").input_shape)
    # print(model.get_layer(name="dense_2").output_shape)
    return model


def train_model(model, X, Y):
    loss = list()

    for eph in range(50):

        for i in range(len(X)):
            # fit model for one epoch on this sequence

            hist = model.fit(np.array(X[i]).reshape(1, len(X[i]), 300), np.array(Y[i]).reshape(1, len(Y[i]), 3),
                             epochs=1, batch_size=1, verbose=0)
            loss.append(hist.history['loss'][0])
        print('Loss after {} epoch is:{}'.format(eph, loss[-1]))
    return loss


def main():
    results = DataFrame()
    x_train, y_train, x_test, y_test = load_data()
    model = get_bi_lstm_model(len(y_train), 'concat')
    results['bilstm_con'] = train_model(model, x_train, y_train)
    model_json = model.to_json()
    with open("../parameters/keras_model_ner.json", "w") as json_file:
        json_file.write(model_json)
    # serialize weights to HDF5
    model.save_weights("../parameters/keras_model_ner.h5")
    print("Saved model to disk")
    # score = model.evaluate(x_test[0], y_test[0], batch_size=1, verbose=1)
    # print('Test score:', score[0])
    # print('Test accuracy:', score[1])




if __name__ == '__main__':
    exec_time = '{:.2f}s'.format(timeit.timeit("main()", setup="from classify_seq_keras_train import main", number=1))
    print(exec_time)
