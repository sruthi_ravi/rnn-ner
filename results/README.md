
### NER for 3 output classes ###

#### Hyper parameters ####

- Train samples - 2394 sentences
- Test samples - 3850 sentences
- Learning rate - 0.003
- Hidden units -  200
- Output units - 3, 7
- Epochs - 50, 100
- Optimizer - Gradient Descent
- Update Type - Sequence, Window (BiRNN with separate FC training)


![alt text](birnn_nofc.jpg)
![alt text](birnn_fc_3class.jpg)
![alt text](forward_rnn.jpg)
![alt text](forward_rnn_epochs.jpg)
![alt text](reverse_rnn_epochs.jpg)
![alt text](birnn_separatefc.jpg)



### NER for 7 output classes ###


![alt text](birnn_fc_7class.jpg)
![alt text](forward_rnn_7.jpg)
![alt text](forward_rnn_window.jpg)
![alt text](reverse_rnn_7.jpg)
![alt text](birnn_separatefc7.jpg)


### Observations ###

- BiRNN with FC layers F1 score improves with increase in number of epochs.
- Single RNN (forward) provides better accuracy than biRNN with FC layers.
- BiRNN with separate FC layers performs better than the single RNN and BiRNN with FC layers.
- With more number of epochs for BiRNN and single RNN, precision improves and recall is almost the same.
- The reverse RNN accuracy is less compared to forward RNN accuracy.
- Increase in bptt window size reduces accuracy for single RNNs forward and reverse.
- Bptt window size of 1 work best than any other window size.
- BiRNN with separate FC layers  provides good accuracy when each of them are run for 100 epochs.
- 3 classes birnn model work better than the 7 classes. (May be due to reduced number of samples in each class in 7 class model)