# README #

### Named Entity Recognition using Birectional RNN ###
This repository has a working model of bidirectional RNN architecture experimented on named entity recognition. The network is trained using NER tagged twitter dataset. The architecture consists of bi-directional RNN layers and FC layers. 
A sentence represents one train instance and at each time instance, a word is fed as input to the RNN and the tag of the word is predicted as output.
During the test phase, the network predicts appropriate tags for the input sentence.
We also experimented on different ways of combining (concat, add, multiply) the bi-RNN output and concatenation works best for this dataset. For sanity check we also configured and ran the keras model for this dataset.

### DataSet ###
* Reference for the dataset - https://github.com/aritter/twitter_nlp/tree/master/data
* The dataset contains set of tagged twitter sentences organized as 'train.txt', 'test.txt', 'dev.txt' (/data/source). 
    - [train/test/dev]_types.txt - ['B-person', 'I-person', 'B-geo-loc', 'I-geo-loc', 'B-company','I-company', Not Named Entity] 
      (only top three classes has been taken for this experiment)
    - [train/test/dev]_notypes.txt- ['B', 'I', Not named entity]
    A new line in the source file defines the start of the new sentence.
* The raw data is preprocessed and word vectors are generated for each word using Google word2vect embedding. Please use this link to download the trained embedding model -https://machinelearningmastery.com/develop-word-embeddings-python-gensim/.
  The code to generate the word embedding is present in /experiments/word2vect.py. After the word vectors are generated, the numpy arrays for train/test and dev are saved in /data/gen/ . The words for which the word embeddings are not available as assigned a randomly generated vector.
  Stand alone files for preprocessing -  
    - /experiments/word2vect - Loads Google word2vect embedding and generated the word vect embedding for the train, test and dev data.
    - /experiments/ner - Parse the raw data, fectch the word2vect emvbedding and saves them to train, test and dev files.


#### Data Sample ####
* /data/gen/train_types
    - Sample X: Its supposed to rain in The         Bay         on Sunday
    - Sample Y:  O    O       O   O   O B-geo-loc   I-geo-loc    O   O

* /data/gen/train_notypes
    - Sample X: Its supposed to rain in The Bay  on Sunday
    - Sample Y:  O    O       O   O   O  B   I    O   O

### Architecture ###
The bi-RNN is created using two single RNNs in forward and reverse direction. The output of the single RNNs are concatenated and passed as 
input to the FC layer. The intermediate layers use Sigmoid activation function and final output layer uses Softmax for classification.

### How do I set up? ###

* Model set up
    - /core contains the files for the char-RNN model architecture.
    - /core/activation - Forward and backward logic for activation functions 
    - /core/gates - RNN gate operations
    - /core/layer - Layer architecture
    - /core/bi_rnn_v2 - Bi-RNN Model architecture with FC with forward and reverse independent RNNs. This is the default architecture as it performs better used for model initialization
    - /core/bi_rnn_concat - Bi-RNN Model architecture with FC with forward and reverse combined RNNs

    
* Experiment set up
    /experiments contains the train and test set up files
    /experiments/helper - Helper files for model initialization and configuration.
    - init_model function - defines the configuration such as input, hidden and output dimension and the hyper-parameters for bi-RNN.
    - The helper file also defines the location of the data and parameters weights to save the weights after training. The same paths are used during test time.  
    - /experiments/ner_train - Loads the train data and invoke the model training.
    - /experiments/ner_test - Loads the test data and invoke the model prediction.
    - /experiments/keras_train - Use the NER datatset and configure the keras bi-RNN network training.
    - /experiments/keras_test - Use the NER datatset and configure the keras bi-RNN network testing.
     
* Output and Parameters
    - /output - saves the accuracy metrics in this directory under /output/ner_output.txt
    - /parameters - saves the model parameters weight matrices files in this directory
    parameter_file = {
    'u': '../parameters/u.npy', # forward RNN input to hidden weight
    'v': '../parameters/v.npy', # forward RNN hidden to output weight
    'w': '../parameters/w.npy', # # forward RNN hidden to hidden weight
    'u_bi': '../parameters/u_bi.npy', # reverse RNN input to hidden weight
    'w_bi': '../parameters/w_bi.npy', # reverse RNN hidden to hidden weight
    'v_bi': '../parameters/v_bi.npy', # reverse RNN hidden to output weight
    'fc_h': '../parameters/fc_h.npy', # FC layer 1 weight
    'fc_hb': '../parameters/fc_hb.npy', # FC layer 1 weight
    'fc_w': '../parameters/fc_w.npy', # FC layer 2 weight
    'fc_b': '../parameters/fc_b.npy', # FC layer 2 bias
}
    - For the keras model, the parameters are saved in 
        - /parameters/keras_model_ner.h5
        - /parameters/keras_model_ner.json
    This path is used to save and read the model in train and test files.
    
* Configurable parameters
    * Epochs
    * Learning rate
    * RNN Input, hidden and output dimension
    * bptt truncate size
    * Optimizer - 'adam' or 'gradient descent'. If nothing is specified default is gradient descent.
    * Parameter update type - 'sequence' or 'window' for end of sequence or window updates.
    * Output activation function 
    * Bi-RNN concatenation size
    * FC layer input, hidden and output dimension
    * Sigmoid + Softmax works better 
    

* How to run
    - To start training, run experiments/ner_train file.
    - To start testing, run the /experiments/ner_test file. Check for the correct saved parameters path in /experiments/helper file.
    - To start training keras model, run experiments/keras_train file.
    - To start testing keras model, run the /experiments/keras_test file.
    