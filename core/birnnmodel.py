from core.model import Model as base
import numpy as np
from core.layer import BiRNNLayer as Layer
from core.gates import MultiplyGate

mulGate = MultiplyGate()


class BiRNN(base):
    def __init__(self, word_dim, output_dim, output_layer, generator, hidden_dim=100, bptt_truncate=4,
                 learning_rate=1e-3, optimizer=None, update_type='sequence', u=None, v=None,
                 w=None, u_bi=None, w_bi=None):
        super(BiRNN, self).__init__(word_dim, output_dim, output_layer, generator, hidden_dim, bptt_truncate,
                                    learning_rate, optimizer, update_type, u, v,
                                    w)
        self.U_bi = u_bi if u_bi is not None else np.random.uniform(-np.sqrt(1. / word_dim), np.sqrt(1. / word_dim),
                                                                    (hidden_dim, word_dim))
        self.W_bi = w_bi if w_bi is not None else np.random.uniform(-np.sqrt(1. / hidden_dim), np.sqrt(1. / hidden_dim),
                                                                    (hidden_dim, hidden_dim))
        self.V = v if v is not None else np.random.uniform(-np.sqrt(1. / hidden_dim), np.sqrt(1. / hidden_dim),
                                                           (output_dim, 2 * hidden_dim))

        self.birnn_s = []
        self.mulv = []

    def forward_propagation(self, x, U, W, V):
        # The total number of time steps
        T = len(x)
        layers = []
        prev_s = np.zeros(self.hidden_dim)
        # For each time step...
        for t in range(T):  # 0- 4
            layer = Layer()
            input = self.generator(x[t], self.word_dim)
            layer.forward(input, prev_s, U, W, V)
            prev_s = layer.s
            layers.append(layer)
        return layers

    def predict(self, x):
        output = self.output_layer()
        fwd, bwd = self.forward(x)
        return np.array([output.get_y(output.predict(m)) for m in self.mulv])

    def concat_layers(self, layer1, layer2):
        T = len(layer1)
        for t in range(T):
            self.birnn_s.append(np.concatenate((layer1[t].s, layer2[t].s), axis=0))
            self.mulv.append(mulGate.forward(self.V, self.birnn_s[t]))

    def concat_layers_bwd(self, layer1, layer2):
        T = len(layer1)
        for t in range(T):
            self.birnn_s[t] = np.concatenate((layer1[t].s, layer2[t].s), axis=0)
            self.mulv[t] = mulGate.forward(self.V, self.birnn_s)

    def forward(self, x):
        self.birnn_s = []
        self.mulv = []
        # remove V its not used

        layers_fd = self.forward_propagation(x, self.U, self.W, self.V)
        x_rev = np.asarray(list(reversed(x)))
        layers_bd = self.forward_propagation(x_rev, self.U_bi, self.W_bi, self.V)
        self.concat_layers(layers_fd, list(reversed(layers_bd)))
        #self.concat_layers(layers_fd, layers_bd)
        return layers_fd, layers_bd

    def bptt_seq(self, x, y):
        assert len(x) == len(y)
        output = self.output_layer()
        layers_fd, layers_bd = self.forward(x)
        dU = np.zeros(self.U.shape)
        dV = np.zeros(self.V.shape)
        dW = np.zeros(self.W.shape)
        dU_bi = np.zeros(self.U_bi.shape)
        dW_bi = np.zeros(self.W_bi.shape)
        x_rev = np.asarray(list(reversed(x)))
        T = len(layers_fd)

        diff_s = np.zeros(self.hidden_dim)

        # perform backpropagation for the the forward direction of the input
        for t in range(0, T):  # T=10
            input = self.generator(x[t], self.word_dim)
            # initialize the start indexes for forward and backward
            f_i = t
            f_s = f_i - 1
            b_i = T - f_i - 1
            b_s = b_i - 1
            prev_s_fd = np.zeros(self.hidden_dim) if f_i == 0 else layers_fd[f_i - 1].s
            prev_s_bd = np.zeros(self.hidden_dim) if b_i == 0 else layers_bd[b_i - 1].s

            if self.update_type is 'window':
                layers_fd[f_i].forward(input, prev_s_fd, self.U, self.W, self.V)
                layers_bd[b_i].forward(input, prev_s_bd, self.U_bi, self.W_bi, self.V)
                self.concat_layers(layers_fd[f_i], layers_bd[b_i])

                predict = output.predict(self.mulv[t])
                dmulv = output.derivative(self.mulv[t], y[t])

            else:
                dmulv = output.derivative(self.mulv[t], y[t])
                #layers_fd[f_i].forward(input, prev_s_fd, self.U, self.W, self.V)
                #layers_bd[b_i].forward(input, prev_s_bd, self.U_bi, self.W_bi, self.V)
                self.birnn_s[t] = np.concatenate((layers_fd[f_i].s, layers_bd[b_i].s), axis=0)
                self.mulv[t] = mulGate.forward(self.V, self.birnn_s[t])
                predict = output.predict(self.mulv[t])

            # print('Predicted final mulv after activation: {}\n'.format(predict))
            dV, dsv_bi = mulGate.backward(self.V, self.birnn_s[t], dmulv)
            dprev_s_fd, dU_t, dW_t, dV_t = layers_fd[f_i].backward(input, prev_s_fd, self.U, self.W,
                                                                 dsv_bi[:self.hidden_dim], diff_s, dV)

            dprev_s_bd, dU_bi_t, dW_bi_t, dV_t = layers_bd[b_i].backward(input, prev_s_bd, self.U_bi, self.W_bi,
                                                                               dsv_bi[self.hidden_dim:], diff_s, dV)

            dmulv = np.zeros(self.output_dim)

            for i in range(f_s, max(-1, t - self.bptt_truncate - 1), -1):
                input = self.generator(x[i], self.word_dim)
                prev_s_i_fd = np.zeros(self.hidden_dim) if i == 0 else layers_fd[i - 1].s
                # this is wrong pass in the ith dmulv here
                dprev_s_fd, dU_i, dW_i, dV_i = layers_fd[i].backward(input, prev_s_i_fd, self.U, self.W,
                                                                     dsv_bi[:self.hidden_dim],
                                                                     dprev_s_fd,
                                                                     dV)

                dU_t += dU_i
                dW_t += dW_i

            for i in range(b_s, max(-1, t - self.bptt_truncate - 1), -1):
                input = self.generator(x_rev[i], self.word_dim)
                prev_s_i_bd = np.zeros(self.hidden_dim) if i == 0 else layers_bd[i - 1].s
                dprev_s_bd, dU_bi_i, dW_bi_i, dV_i = layers_bd[i].backward(input, prev_s_i_bd, self.U_bi, self.W_bi,
                                                                           dsv_bi[self.hidden_dim:], dprev_s_bd,
                                                                           dV)
                dU_bi_t += dU_bi_i
                dW_bi_t += dW_bi_i

            if self.update_type is 'window':
                dV = dV_t
                dU = dU_t
                dW = dW_t
                dU_bi = dU_bi_i
                dW_bi = dW_bi_i

                self.update_params(
                    {'grads': {'V': dV, 'U': dU, 'W': dW, 'U_bi': dU_bi, 'W_bi': dW_bi},
                     'params': {'U': self.U, 'V': self.V, 'W': self.W, 'U_bi': self.U_bi, 'W_bi': self.W_bi}},
                )
            else:
                dV += dV_t
                dU += dU_t
                dW += dW_t
                dU_bi += dU_bi_t
                dW_bi += dW_bi_t

        return dU, dW, dV, dU_bi, dW_bi

    def sgd_step(self, x, y):
        dU, dW, dV, dU_bi, dW_bi = self.bptt(x, y)
        for dparam in [dU, dW, dV, dU_bi, dW_bi]:
            np.clip(dparam, -5, 5, out=dparam)
        if self.update_type is not 'window':
            self.update_params(
                {'grads': {'V': dV, 'U': dU, 'W': dW, 'U_bi': dU_bi, 'W_bi': dW_bi},
                 'params': {'U': self.U, 'V': self.V, 'W': self.W, 'U_bi': self.U_bi, 'W_bi': self.W_bi}}, type='adam')

    def update_params(self, parameters, type=None):
        if type == 'adam':
            self.adam_optimizer(parameters)
        else:
            grads = parameters['grads']
            weights = parameters['params']
            weights['U'] -= self.learning_rate * grads['U']
            weights['V'] -= self.learning_rate * grads['V']
            weights['W'] -= self.learning_rate * grads['W']
            weights['U_bi'] -= self.learning_rate * grads['U_bi']
            weights['W_bi'] -= self.learning_rate * grads['W_bi']

    def calculate_loss(self, x, y):
        assert len(x) == len(y)
        output = self.output_layer()
        _, _ = self.forward(x)
        loss = 0.0
        for i, m in enumerate(self.mulv):
            loss += output.loss(m, y[i])
        return loss / float(len(y))

    def calculate_total_loss(self, X, Y):
        loss = 0.0
        for i in range(len(Y)):
            loss += self.calculate_loss(X[i], Y[i])
        return loss / float(len(Y))

    def train(self, X, Y, nepoch=100, evaluate_loss_after=5):
        num_examples_seen = 0
        losses = []
        for epoch in range(nepoch):
            if epoch % evaluate_loss_after == 0:
                loss = self.calculate_total_loss(X, Y)
                losses.append((num_examples_seen, loss))

                print(
                    "Loss after num_examples_seen={0} epoch={1}: {2}".format(num_examples_seen, epoch, loss))
            for i in range(len(Y)):
                self.sgd_step(X[i], Y[i])
                num_examples_seen += 1
        return losses
