from core.model import Model as base
import numpy as np
from core.layer import RNNLayer as Layer
from core.layer import fc as FC
from core.gates import MultiplyGate

mulGate = MultiplyGate()


class BiRNN(base):
    def __init__(self, word_dim, output_dim, output_layer, generator, hidden_dim=100, bptt_truncate=4,
                 learning_rate=1e-3, optimizer=None, update_type='sequence', weights={}):
        super(BiRNN, self).__init__(word_dim, output_dim, output_layer, generator, hidden_dim, bptt_truncate,
                                    learning_rate, optimizer, update_type, weights)
        self.v_dim = 3
        self.U_bi = weights['u_bi'] if 'u_bi' in weights else np.random.uniform(-np.sqrt(1. / word_dim),
                                                                                np.sqrt(1. / word_dim),
                                                                                (hidden_dim, word_dim))
        self.W_bi = weights['w_bi'] if 'w_bi' in weights else np.random.uniform(-np.sqrt(1. / hidden_dim),
                                                                                np.sqrt(1. / hidden_dim),
                                                                                (hidden_dim, hidden_dim))
        self.V = weights['v'] if 'v' in weights else np.random.uniform(-np.sqrt(1. / hidden_dim),
                                                                       np.sqrt(1. / hidden_dim),
                                                                       (self.v_dim, hidden_dim))
        self.V_bi = weights['v_bi'] if 'v_bi' in weights else np.random.uniform(-np.sqrt(1. / hidden_dim),
                                                                                np.sqrt(1. / hidden_dim),
                                                                                (self.v_dim, hidden_dim))

        self.fc_input_dim = 2 * self.v_dim
        self.fc_hidden_dim = 50
        self.fc_output_dim = output_dim
        # input_size = self.input_bz * np.prod(self.fc_input_dim)
        # weight_size = self.fc_output_dim * np.prod(self.fc_input_dim)
        fc_w = weights['fc_w'] if 'fc_w' in weights else None
        fc_b = weights['fc_b'] if 'fc_b' in weights else None
        fc_h = weights['fc_h'] if 'fc_h' in weights else None
        fc_hb = weights['fc_hb'] if 'fc_hb' in weights else None
        # self.fc_layer = FC(self.fc_input_dim, self.fc_output_dim, init_scale=0.002, name="fc_test", fc_w=fc_w,
        #                    fc_b=fc_b)
        self.fc_layer = FC(self.fc_input_dim, self.fc_hidden_dim, self.fc_output_dim, init_scale=0.002, name="fc_test",
                           fc_w=fc_w, fc_b=fc_b, fc_h=fc_h, fc_hb=fc_hb)
        self.birnn_op = []
        print(self.bptt_truncate)

    def forward_propagation(self, x, U, W, V):
        # The total number of time steps
        T = len(x)
        layers = []
        prev_s = np.zeros(self.hidden_dim)
        # For each time step...
        for t in range(T):  # 0- 4
            layer = Layer()
            input = self.generator(x[t], self.word_dim)
            layer.forward(input, prev_s, U, W, V)
            prev_s = layer.s
            layers.append(layer)
        return layers

    def predict(self, x):
        output = self.output_layer()
        fwd, bwd, fc = self.forward(x)
        return np.array([output.get_y(output.predict(m)) for m in fc])

    def concat_layers(self, layer1, layer2):
        self.birnn_op = []
        T = len(layer1)
        for t in range(T):
            self.birnn_op.append(np.concatenate((layer1[t].mulv, layer2[t].mulv), axis=0))
        self.birnn_op = np.array(self.birnn_op)

    def forward(self, x):
        # remove V its not used
        self.input_bz = len(x)
        layers_fd = self.forward_propagation(x, self.U, self.W, self.V)
        x_rev = np.asarray(list(reversed(x)))
        layers_bd = self.forward_propagation(x_rev, self.U_bi, self.W_bi, self.V_bi)
        self.concat_layers(layers_fd, list(reversed(layers_bd)))

        # Test the fc forward function

        fc_out = self.fc_layer.forward(self.birnn_op)

        # self.concat_layers(layers_fd, layers_bd)
        return layers_fd, layers_bd, fc_out

    def bptt_seq(self, x, y):
        assert len(x) == len(y)
        output = self.output_layer()
        layers_fd, layers_bd, fc_out = self.forward(x)
        dout = []
        for i in range(len(x)):
            dout.append(output.derivative(fc_out[i], y[i]))
        dbirnn = self.fc_layer.backward(dout).reshape(self.input_bz, 2 * self.v_dim)
        dU = np.zeros(self.U.shape)
        dV = np.zeros(self.V.shape)
        dW = np.zeros(self.W.shape)
        dU_bi = np.zeros(self.U_bi.shape)
        dW_bi = np.zeros(self.W_bi.shape)
        dV_bi = np.zeros(self.V_bi.shape)
        x_rev = np.asarray(list(reversed(x)))
        T = len(layers_fd)

        diff_s = np.zeros(self.hidden_dim)

        # perform backpropagation for the the forward direction of the input
        for t in range(0, T):  # T=10
            input = self.generator(x[t], self.word_dim)
            # initialize the start indexes for forward and backward
            f_i = t
            f_s = f_i - 1
            b_i = T - f_i - 1
            b_s = b_i - 1
            prev_s_fd = np.zeros(self.hidden_dim) if f_i == 0 else layers_fd[f_i - 1].s
            prev_s_bd = np.zeros(self.hidden_dim) if b_i == 0 else layers_bd[b_i - 1].s
            predict = output.predict(fc_out[t])

            # layers_fd[f_i].forward(input, prev_s_fd, self.U, self.W, self.V)
            # layers_bd[b_i].forward(input, prev_s_bd, self.U_bi, self.W_bi, self.V)
            # self.birnn_s[t] = np.concatenate((layers_fd[f_i].s, layers_bd[b_i].s), axis=0)
            # self.mulv[t] = mulGate.forward(self.V, self.birnn_s[t])


            # print('Predicted final mulv after activation: {}\n'.format(predict))
            f_dmulv = dbirnn[t][:self.v_dim]
            b_dmulv = dbirnn[t][self.v_dim:]

            dprev_s_fd, dU_t, dW_t, dV_t = layers_fd[f_i].backward(input, prev_s_fd, self.U, self.W, self.V, diff_s,
                                                                   f_dmulv)

            dprev_s_bd, dU_bi_t, dW_bi_t, dV_bi_t = layers_bd[b_i].backward(input, prev_s_bd, self.U_bi, self.W_bi,
                                                                            self.V_bi,
                                                                            diff_s, b_dmulv)

            dmulv = np.zeros(self.v_dim)

            for i in range(f_s, max(-1, f_i - self.bptt_truncate - 1), -1):
                input = self.generator(x[i], self.word_dim)
                prev_s_i_fd = np.zeros(self.hidden_dim) if i == 0 else layers_fd[i - 1].s
                # this is wrong pass in the ith dmulv here
                dprev_s_fd, dU_i, dW_i, dV_i = layers_fd[i].backward(input, prev_s_i_fd, self.U, self.W, self.V,
                                                                     dprev_s_fd,
                                                                     dmulv)

                dU_t += dU_i
                dW_t += dW_i

            for i in range(b_s, max(-1, b_i - self.bptt_truncate - 1), -1):
                input = self.generator(x_rev[i], self.word_dim)
                prev_s_i_bd = np.zeros(self.hidden_dim) if i == 0 else layers_bd[i - 1].s
                dprev_s_bd, dU_bi_i, dW_bi_i, dV_i = layers_bd[i].backward(input, prev_s_i_bd, self.U_bi, self.W_bi,
                                                                           self.V_bi, dprev_s_bd, dmulv)

                dU_bi_t += dU_bi_i
                dW_bi_t += dW_bi_i

            dV += dV_t
            dU += dU_t
            dW += dW_t
            dU_bi += dU_bi_t
            dW_bi += dW_bi_t
            dV_bi += dV_bi_t

        return dU, dW, dV, dU_bi, dW_bi, dV_bi

    def sgd_step(self, x, y):
        dU, dW, dV, dU_bi, dW_bi, dV_bi = self.bptt(x, y)
        for dparam in [dU, dW, dV, dU_bi, dW_bi, dV_bi]:
            np.clip(dparam, -5, 5, out=dparam)
        if self.update_type is not 'window':
            self.update_params(
                {'grads': {'V': dV, 'U': dU, 'W': dW, 'U_bi': dU_bi, 'W_bi': dW_bi, 'V_bi': dV_bi},
                 'params': {'U': self.U, 'V': self.V, 'W': self.W, 'U_bi': self.U_bi, 'W_bi': self.W_bi,
                            'V_bi': self.V_bi}}, type=None)

    def update_params(self, parameters, type=None):
        if type == 'adam':
            self.adam_optimizer(parameters)
        else:
            grads = parameters['grads']
            weights = parameters['params']
            weights['U'] -= self.learning_rate * grads['U']
            weights['V'] -= self.learning_rate * grads['V']
            weights['W'] -= self.learning_rate * grads['W']

            weights['U_bi'] -= self.learning_rate * grads['U_bi']
            weights['W_bi'] -= self.learning_rate * grads['W_bi']
            weights['V_bi'] -= self.learning_rate * grads['V_bi']

            self.fc_layer.params['fc_test_w'] -= self.learning_rate * self.fc_layer.grads['fc_test_w']
            self.fc_layer.params['fc_test_b'] -= self.learning_rate * self.fc_layer.grads['fc_test_b']
            self.fc_layer.params['fc_test_h'] -= self.learning_rate * self.fc_layer.grads['fc_test_h']
            self.fc_layer.params['fc_test_hb'] -= self.learning_rate * self.fc_layer.grads['fc_test_hb']

    def calculate_loss(self, x, y):
        assert len(x) == len(y)
        output = self.output_layer()
        fwd, bwd, fc = self.forward(x)
        loss = 0.0
        for i, m in enumerate(fc):
            loss += output.loss(m, y[i])
        return loss / float(len(y))

    def calculate_total_loss(self, X, Y):
        loss = 0.0
        for i in range(len(Y)):
            loss += self.calculate_loss(X[i], Y[i])
        return loss / float(len(Y))

    def train(self, X, Y, nepoch=100, evaluate_loss_after=5):
        num_examples_seen = 0
        losses = []
        for epoch in range(nepoch):
            if epoch % evaluate_loss_after == 0:
                loss = self.calculate_total_loss(X, Y)
                losses.append((num_examples_seen, loss))

                print(
                    "Loss after num_examples_seen={0} epoch={1}: {2}".format(num_examples_seen, epoch, loss))
            for i in range(len(Y)):
                self.sgd_step(X[i], Y[i])
                num_examples_seen += 1
        return losses
