import numpy as np
import random


class Sigmoid:
    def forward(self, x):
        return 1.0 / (1.0 + np.exp(-x))

    def backward(self, x, top_diff):
        output = self.forward(x)
        return (1.0 - output) * output * top_diff

    def loss(self, x, y):
        output = self.forward(x)
        return (1.0 - output) * output * (output[0] - y)

    def derivative(self, x, y):
        output = self.forward(x)
        return (1.0 - output) * output * (output[0] - y)

    def predict(self, x):
        return 1.0 / (1.0 + np.exp(-x))

    def get_y(self, y):
        return y[0]


class SigmoidwithCrossEntropy:
    def predict(self, x):
        return 1.0 / (1.0 + np.exp(-x))  # ypred

    # cross entropy loss
    def loss(self, x, y):
        # sum of -y log y geberal cross entropy loss
        probs = self.predict(x)
        return - (y * np.log(probs[0]) + (1 - y) * np.log(1 - probs[0]))

    def derivative(self, x, y):
        # dE = e-x/(1+e-x(2) = 1/(ypred)2
        # y- ypred
        # sigmoid(ypred, y)
        # probs = predict(x)
        # (1.0 - probs) * probs * (ypred-y)

        probs = self.predict(x)
        return np.array([probs[0] - y])

    def get_y(self, y):
        return y[0]


class Tanh:
    def forward(self, x):
        return np.tanh(x)

    def backward(self, x, top_diff):
        output = self.forward(x)
        return (1.0 - np.square(output)) * top_diff


class Softmax:
    def predict(self, x):
        exp_scores = np.exp(x)
        return exp_scores / np.sum(exp_scores)

    # cross entropy loss
    def loss(self, x, y):
        probs = self.predict(x)
        return -np.log(probs[y])

    # softmax with cross entropy loss
    def derivative(self, x, y):
        probs = self.predict(x)
        # -(y-ypred)
        # one hot encoding of y = 2 [0,0,1] #  refactorrrrr
        probs[y] -= 1.0
        return probs

    def get_y(self, y):
        return np.argmax(y)

    def random_y(self, y):
        # y_pred = np.argwhere(np.logical_and(y>=0.1, y <= 0.6))
        # return random.choice(y_pred)[0] if len(y_pred)!=0 else np.argmax(y)

        prob = np.random.choice(54, 1, p=y)[0]
        return prob


    def get_y_at_index(self, y, index):
        return y[index]


class Regression:
    def predict(self, x):
        return x

    def loss(self, x, y):
        return ((y - x) ** 2) / 2

    def derivative(self, x, y):
        return -(y - x)

    def get_y(self, y):
        return y[0]
